import { Injectable } from '@angular/core';
import { Place } from './place.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  private _places: Place[] = [
    new Place('p1', 'Manhattan mansion', 'description', 'aaaa', 149.99),
    new Place('p2', 'Ladefef mansion', 'description2', 'bbbb', 49.99),
    new Place('p3', 'Goffii mansion', 'description3', 'cccc', 159.99)
  ];

  get places() {
    return [...this._places];
  }

  constructor() { }
}
